Table Creation Scripts

CREATE TABLE public.ckgroup
(
    id integer NOT NULL DEFAULT nextval('ckgroup_id_seq'::regclass),
    groupname text COLLATE pg_catalog."default",
    groupdetails text COLLATE pg_catalog."default",
    fromdate date,
    todate date,
    status text COLLATE pg_catalog."default",
    createdby text COLLATE pg_catalog."default",
    createddate date,
    CONSTRAINT ckgroup_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.ckgroup
    OWNER to postgres;


CREATE TABLE public.cklocation
(
    id integer NOT NULL DEFAULT nextval('cklocation_id_seq'::regclass),
    locationname text COLLATE pg_catalog."default",
    CONSTRAINT cklocation_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.cklocation

CREATE TABLE public.cklogin
(
    id integer NOT NULL DEFAULT nextval('cklogin_id_seq'::regclass),
    userid integer,
    roleid integer,
    token text COLLATE pg_catalog."default",
    CONSTRAINT cklogin_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.cklogin
    OWNER to postgres;

CREATE TABLE public.ckmessage
(
    groupid bigint,
    message text COLLATE pg_catalog."default",
    fromdate date,
    todate date,
    id integer NOT NULL DEFAULT nextval('ckmessage_id_seq'::regclass),
    CONSTRAINT ckmessage_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.ckmessage
    OWNER to postgres;

CREATE TABLE public.ckrole
(
    id integer NOT NULL DEFAULT nextval('ckrole_id_seq'::regclass),
    rolename text COLLATE pg_catalog."default",
    CONSTRAINT ckrole_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.ckrole
    OWNER to postgres;


CREATE TABLE public.ckskill
(
    id integer NOT NULL DEFAULT nextval('ckskill_id_seq'::regclass),
    skillname text COLLATE pg_catalog."default",
    CONSTRAINT ckskill_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.ckskill
    OWNER to postgres;


CREATE TABLE public.ckuser
(
    id integer NOT NULL DEFAULT nextval('ckuser_id_seq'::regclass),
    name text COLLATE pg_catalog."default",
    gender text COLLATE pg_catalog."default",
    dob date,
    mobile text COLLATE pg_catalog."default",
    district text COLLATE pg_catalog."default",
    address text COLLATE pg_catalog."default",
    email text COLLATE pg_catalog."default",
    profession text COLLATE pg_catalog."default",
    institution text COLLATE pg_catalog."default",
    availabletimings text COLLATE pg_catalog."default",
    joiningdate date,
    password text COLLATE pg_catalog."default",
    skill text COLLATE pg_catalog."default",
    CONSTRAINT ckuser_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.ckuser
    OWNER to postgres;


CREATE TABLE public.ckusergroup
(
    id integer NOT NULL DEFAULT nextval('ckusergroup_id_seq'::regclass),
    userid integer,
    groupid integer,
    CONSTRAINT ckusergroup_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.ckusergroup
    OWNER to postgres;

CREATE TABLE public.ckuserrole
(
    id integer NOT NULL DEFAULT nextval('ckuserrole_id_seq'::regclass),
    userid integer,
    roleid integer,
    CONSTRAINT ckuserrole_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.ckuserrole
    OWNER to postgres;

CREATE TABLE public.ckuserskill
(
    id integer NOT NULL DEFAULT nextval('ckuserskill_id_seq'::regclass),
    userid integer,
    skillid integer,
    CONSTRAINT ckuserskill_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.ckuserskill
    OWNER to postgres;


    OWNER to postgres;