/**
 * 
 */
package com.ck.utils;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author Vinod_S08
 *
 */
public class CryptoHelper {
	
	private static String key = "Bar12345Bar12345"; // 128 bit key
    // Create key and cipher
    private static Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
    
    // encrypt the text
    
    
    public static String encrypt(String plainText) {
    	try {
    		Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
			byte[] encrypted = cipher.doFinal(plainText.getBytes());
			byte[] encodedBytes = Base64.getEncoder().encode(encrypted);			
            return (new String(encodedBytes));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
    
    public static String decrypt(String encrypted) {
    	try {
    		byte[] decodedBytes = Base64.getDecoder().decode(encrypted.getBytes());
			
    		Cipher cipher = Cipher.getInstance("AES");
    		cipher.init(Cipher.DECRYPT_MODE, aesKey);
            String decrypted = new String(cipher.doFinal(decodedBytes));
            return decrypted;
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	return null;
    }

}
