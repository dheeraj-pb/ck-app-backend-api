package com.ck.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;

import org.joda.time.DateTime;
import org.joda.time.base.BaseDateTime;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ck.models.*;
import com.ck.services.CkService;
import com.ck.utils.CryptoHelper;



@RestController
@RequestMapping("/api")
public class CkController {
	
	private static final String JSON_TYPE = "application/json";
	private CkService ckService;
	
//	private Bucket bucket;
	
//	@Autowired
//	public VolunteersController(Bucket bucket) {
//		this.bucket = bucket;
//	}
	
	public CkController(CkService ckService) {
		this.ckService = ckService;
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST, consumes = {JSON_TYPE})
	@ResponseBody
	public ResponseEntity<?> login(@RequestBody LoginRequest request){
		WrapperResponse wResponse = new WrapperResponse();
		try {
			System.out.println("The application is started..the request hit.."+request);
			String token = ckService.login(request);
			LoginResponse response = new LoginResponse();
			response.setToken(token);			
			wResponse = new WrapperResponse();
			wResponse.setStatus(HttpStatus.OK.toString());
			wResponse.setPayload(response);			
			return ResponseEntity.ok(wResponse);
		}catch(Exception e) {
			//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(e.getMessage());
		}		
	}
	
	@RequestMapping(value="/listGroupsWithMessageForUser/{userId}",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> listGroupsWithMessageForUser(@PathVariable Integer userId){
		try {
			System.out.println("The application is started..the request hit.."+userId);
			List<GroupWithMessageAndParticipants> modelList = ckService.listGroupsWithMessageForUser(userId);
			ListGroupsWithMessageResponse response = new ListGroupsWithMessageResponse();
			response.setGroups(modelList);
			WrapperResponse wResponse = new WrapperResponse();
			wResponse.setStatus("200");
			wResponse.setPayload(response);
			return ResponseEntity.ok(wResponse);
		}catch(Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(e.getMessage());
		}		
	}
	
	@RequestMapping(value="/listVolunteers",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> searchVolunteers(){
		try {
			System.out.println("The application is started..the request hit..");
			List<VolunteerBasicModel> modelList = ckService.getAllVolunteers();
			VolunteerSearchResponse response = new VolunteerSearchResponse();
			response.setVolunteers(modelList);
			WrapperResponse wResponse = new WrapperResponse();
			wResponse.setStatus("200");
			wResponse.setPayload(response);
			return ResponseEntity.ok(wResponse);
		}catch(Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(e.getMessage());
		}		
	}
	
	@RequestMapping(value="/listGroups",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> searchGroups(){
		try {
			System.out.println("The application is started..the request hit..");
			List<Group> listGroups = ckService.getAllGroups();
			ListGroupsResponse response = new ListGroupsResponse();
			response.setGroups(listGroups);
			WrapperResponse wResponse = new WrapperResponse();
			wResponse.setStatus("200");
			wResponse.setPayload(response);
			return ResponseEntity.ok(wResponse);
		}catch(Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(e.getMessage());
		}		
	}
	
	@RequestMapping(value="/getVolunteerProfile/{userId}",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getVolunteerProfile(@PathVariable Integer userId){
		try {
			System.out.println("The application is started..the request hit.."+userId);
			Volunteer volunteer = ckService.getVolunteerProfile(userId);
			VolunteerProfileSearchResponse response = new VolunteerProfileSearchResponse();
			response.setVolunteer(volunteer);
			WrapperResponse wResponse = new WrapperResponse();
			wResponse.setStatus("200");
			wResponse.setPayload(response);
			
			return ResponseEntity.ok(wResponse);
		}catch(Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(e.getMessage());
		}		
	}
	
	@RequestMapping(value="/getLocations",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getLocations(){
		try {
			System.out.println("The application is started..the request hit..");
			List<Location> locations = ckService.getLocations();
			GetLocationsResponse response = new GetLocationsResponse();
			response.setLocations(locations);
			WrapperResponse wResponse = new WrapperResponse();
			wResponse.setStatus("200");
			wResponse.setPayload(response);
			
			return ResponseEntity.ok(wResponse);
		}catch(Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(e.getMessage());
		}		
	}
	
	@RequestMapping(value="/getSkills",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getSkills(){
		try {
			System.out.println("The application is started..the request hit..");
			List<Skill> skills = ckService.getSkills();
			GetSkillsResponse response = new GetSkillsResponse();
			response.setSkills(skills);
			WrapperResponse wResponse = new WrapperResponse();
			wResponse.setStatus("200");
			wResponse.setPayload(response);
			
			return ResponseEntity.ok(wResponse);
		}catch(Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(e.getMessage());
		}		
	}
	
	@RequestMapping(value="/getGroupDetails/{groupId}",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getGroupDetails(@PathVariable Integer groupId){
		WrapperResponse wResponse = new WrapperResponse();
		try {
			System.out.println("The application is started..the request hit.."+groupId);
			Group group = ckService.getGroupDetails(groupId);
			GetGroupDetailsResponse response = new GetGroupDetailsResponse();
			response.setGroup(group);
			wResponse = new WrapperResponse();
			wResponse.setStatus(HttpStatus.OK.toString());
			wResponse.setPayload(response);			
			return ResponseEntity.ok(wResponse);
		}catch(Exception e) {
			//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(e.getMessage());
		}		
	}
	
	@RequestMapping(value="/listUsersForGroup/{groupId}",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> listUsersForGroup(@PathVariable Integer groupId){
		WrapperResponse wResponse = new WrapperResponse();
		try {
			System.out.println("The application is started..the request hit.."+groupId);
			List<Volunteer> users = ckService.listUsersForGroup(groupId);
			ListUsersForGroupResponse response = new ListUsersForGroupResponse();
			response.setGroupId(groupId);
			response.setUsers(users);
			wResponse = new WrapperResponse();
			wResponse.setStatus(HttpStatus.OK.toString());
			wResponse.setPayload(response);			
			return ResponseEntity.ok(wResponse);
		}catch(Exception e) {
			//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(e.getMessage());
		}		
	}
	
	@RequestMapping(value="/getMessages/{groupId}",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getMessagesForGroup(@PathVariable Integer groupId){
		WrapperResponse wResponse = new WrapperResponse();
		try {
			System.out.println("The application is started..the request hit.."+groupId);
			List<Message> messages = ckService.getMessagesForGroup(groupId);
			GetMessagesForGroupResponse response = new GetMessagesForGroupResponse();
			response.setMessages(messages);
			wResponse = new WrapperResponse();
			wResponse.setStatus(HttpStatus.OK.toString());
			wResponse.setPayload(response);			
			return ResponseEntity.ok(wResponse);
		}catch(Exception e) {
			//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(e.getMessage());
		}		
	}
	
	@RequestMapping(value="/getMessagesValidForToday/{groupId}",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getMessagesValidForToday(@PathVariable Integer groupId){
		WrapperResponse wResponse = new WrapperResponse();
		try {
			System.out.println("The application is started..the request hit.."+groupId);
			List<Message> messages = ckService.getMessagesValidForToday(groupId);
			GetMessagesForGroupResponse response = new GetMessagesForGroupResponse();
			response.setMessages(messages);
			wResponse = new WrapperResponse();
			wResponse.setStatus(HttpStatus.OK.toString());
			wResponse.setPayload(response);			
			return ResponseEntity.ok(wResponse);
		}catch(Exception e) {
			//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(e.getMessage());
		}		
	}
	
	@RequestMapping(value="/getMessagesValidForDate/{groupId}/{date}",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getMessagesValidForToday(@PathVariable Integer groupId,@PathVariable String date){
		WrapperResponse wResponse = new WrapperResponse();
		try {
			System.out.println("The incoming date is"+date);
			Calendar cal = Calendar.getInstance();
			cal.set(Integer.parseInt(date.substring(0, 4)), Integer.parseInt(date.substring(4,6))-1, Integer.parseInt(date.substring(6)));
			Date messageDate = new Date(cal.getTimeInMillis());
			System.out.println("The application is started..the request hit.."+groupId+" and date is"+messageDate);
			List<Message> messages = ckService.getMessagesValidForDate(groupId,messageDate);
			GetMessagesForGroupResponse response = new GetMessagesForGroupResponse();
			response.setMessages(messages);
			wResponse = new WrapperResponse();
			wResponse.setStatus(HttpStatus.OK.toString());
			wResponse.setPayload(response);			
			return ResponseEntity.ok(wResponse);
		}catch(Exception e) {
			//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(e.getMessage());
		}		
	}
	
	@RequestMapping(value="/addUser",method=RequestMethod.POST, consumes = {JSON_TYPE})
	@ResponseBody
	public ResponseEntity<?> addUser(@RequestHeader(value="Authorization") String bearerToken,@RequestBody AddVolunteerRequest request){
		if(canProceed(bearerToken)) {
			WrapperResponse wResponse = new WrapperResponse();		
			try {
				System.out.println("The application is started..the request hit.."+request);
				Long lResult = ckService.addUser(request);
				AddVolunteerResponse response = new AddVolunteerResponse();
				response.setId(lResult);
				response.setMessage("Volunteer successfully added");
				wResponse = new WrapperResponse();
				wResponse.setStatus(HttpStatus.OK.toString());
				wResponse.setPayload(response);			
				return ResponseEntity.ok(wResponse);
			}catch(Exception e) {
				//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
				//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
						body(e.getMessage());
			}		
		}else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).
					body("UnAuthorized Attempt");
		}
		
	}
	
	
	@RequestMapping(value="/addGroup",method=RequestMethod.POST, consumes = {JSON_TYPE})
	@ResponseBody
	public ResponseEntity<?> addGroup(@RequestHeader(value="Authorization") String bearerToken,@RequestBody AddGroupRequest request){
		if(canProceed(bearerToken)) {
			WrapperResponse wResponse = new WrapperResponse();
			try {
				System.out.println("The application is started..the request hit.."+request);
				Long lResult = ckService.addGroup(request);
				AddVolunteerResponse response = new AddVolunteerResponse();
				response.setId(lResult);
				response.setMessage("Group successfully added");
				wResponse = new WrapperResponse();
				wResponse.setStatus(HttpStatus.OK.toString());
				wResponse.setPayload(response);			
				return ResponseEntity.ok(wResponse);
			}catch(Exception e) {
				//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
				//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
						body(e.getMessage());
			}		
		}else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).
					body("UnAuthorized Attempt");
		}
		
	}
	
	@RequestMapping(value="/assignUsersToGroup",method=RequestMethod.POST, consumes = {JSON_TYPE})
	@ResponseBody
	public ResponseEntity<?> assignUsersToGroup(@RequestHeader(value="Authorization") String bearerToken,@RequestBody AssignUserToGroupRequest request){
		if(canProceed(bearerToken)) {
			WrapperResponse wResponse = new WrapperResponse();
			try {
				System.out.println("The application is started..the request hit.."+request);
				Long lResult = ckService.assignUsersToGroup(request);
				AssignUserToGroupResponse response = new AssignUserToGroupResponse();
				response.setId(lResult);
				response.setMessage("User to Group successfully added");
				wResponse = new WrapperResponse();
				wResponse.setStatus(HttpStatus.OK.toString());
				wResponse.setPayload(response);			
				return ResponseEntity.ok(wResponse);
			}catch(Exception e) {
				//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
				//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
						body(e.getMessage());
			}		
		}else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).
					body("UnAuthorized Attempt");
		}
		
	}
	
	@RequestMapping(value="/assignGroupsToUser",method=RequestMethod.POST, consumes = {JSON_TYPE})
	@ResponseBody
	public ResponseEntity<?> assignGroupsToUser(@RequestHeader(value="Authorization") String bearerToken,@RequestBody AssignGroupsToUserRequest request){
		if(canProceed(bearerToken)) {
			WrapperResponse wResponse = new WrapperResponse();
			try {
				System.out.println("The application is started..the request hit.."+request);
				Long lResult = ckService.assignGroupsToUser(request);
				AssignGroupsToUserResponse response = new AssignGroupsToUserResponse();
				response.setId(lResult);
				response.setMessage("Groups assigned successfully to the user");
				wResponse = new WrapperResponse();
				wResponse.setStatus(HttpStatus.OK.toString());
				wResponse.setPayload(response);			
				return ResponseEntity.ok(wResponse);
			}catch(Exception e) {
				//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
				//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
						body(e.getMessage());
			}		
		}else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).
					body("UnAuthorized Attempt");
		}
		
	}
	@RequestMapping(value="/removeUserFromGroup",method=RequestMethod.POST, consumes = {JSON_TYPE})
	@ResponseBody
	public ResponseEntity<?> removeUserFromGroup(@RequestHeader(value="Authorization") String bearerToken,@RequestBody RemoveUserFromGroupRequest request){
		if(canProceed(bearerToken)) {
			WrapperResponse wResponse = new WrapperResponse();
			try {
				System.out.println("The application is started..the request hit.."+request);
				ckService.removeUserFromGroup(request);
				RemoveUserFromGroupResponse response = new RemoveUserFromGroupResponse();			
				response.setMessage("User removed from Group successfully");
				wResponse = new WrapperResponse();
				wResponse.setStatus(HttpStatus.OK.toString());
				wResponse.setPayload(response);			
				return ResponseEntity.ok(wResponse);
			}catch(Exception e) {
				//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
				//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
						body(e.getMessage());
			}		
		}else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).
					body("UnAuthorized Attempt");
		}
		
	}
	@RequestMapping(value="/assignRoleToUser",method=RequestMethod.POST, consumes = {JSON_TYPE})
	@ResponseBody
	public ResponseEntity<?> assignRoleToUser(@RequestHeader(value="Authorization") String bearerToken,@RequestBody AssignRoleRequest request){
		if(canProceed(bearerToken)) {
			WrapperResponse wResponse = new WrapperResponse();
			try {
				System.out.println("The application is started..the request hit.."+request);
				Long lResult = ckService.assignRoleToUser(request);
				AssignRoleResponse response = new AssignRoleResponse();
				response.setId(lResult);
				response.setMessage("Role assigned to User successfully");
				wResponse = new WrapperResponse();
				wResponse.setStatus(HttpStatus.OK.toString());
				wResponse.setPayload(response);			
				return ResponseEntity.ok(wResponse);
			}catch(Exception e) {
				//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
				//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
						body(e.getMessage());
			}		
		}else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).
					body("UnAuthorized Attempt");
		}
		
	}
	
	@RequestMapping(value="/removeRoleFromUser",method=RequestMethod.POST, consumes = {JSON_TYPE})
	@ResponseBody
	public ResponseEntity<?> removeRoleFromUser(@RequestHeader(value="Authorization") String bearerToken,@RequestBody RemoveRoleFromUserRequest request){
		if(canProceed(bearerToken)) {
			WrapperResponse wResponse = new WrapperResponse();
			try {
				System.out.println("The application is started..the request hit.."+request);
				ckService.removeRoleFromUser(request);
				RemoveRoleFromUserResponse response = new RemoveRoleFromUserResponse();			
				response.setMessage("Role removed from User successfully");
				wResponse = new WrapperResponse();
				wResponse.setStatus(HttpStatus.OK.toString());
				wResponse.setPayload(response);			
				return ResponseEntity.ok(wResponse);
			}catch(Exception e) {
				//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
				//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
						body(e.getMessage());
			}		
		}else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).
					body("UnAuthorized Attempt");
		}
		
	}
	
	@RequestMapping(value="/broadcastMessage",method=RequestMethod.POST, consumes = {JSON_TYPE})
	@ResponseBody
	public ResponseEntity<?> broadcastMessage(@RequestHeader(value="Authorization") String bearerToken,@RequestBody BroadcastMessageRequest request){
		if(canProceed(bearerToken)) {
			WrapperResponse wResponse = new WrapperResponse();
			try {
				System.out.println("The application is started..the request hit.."+request);
				Long lResult = ckService.broadcastMessageToGroup(request);
				AddGroupResponse response = new AddGroupResponse();
				response.setId(lResult);
				response.setMessage("Broadcast successfully done");
				wResponse = new WrapperResponse();
				wResponse.setStatus(HttpStatus.OK.toString());
				wResponse.setPayload(response);			
				return ResponseEntity.ok(wResponse);
			}catch(Exception e) {
				//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
				//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
						body(e.getMessage());
			}		
		}else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).
					body("UnAuthorized Attempt");
		}
		
	}
	
	
	@RequestMapping(value="/update/{tableName}/{current}/{finalNumber}",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> updateSerialNumbers(@PathVariable String tableName,@PathVariable Integer current,
			@PathVariable Integer finalNumber){
		WrapperResponse wResponse = new WrapperResponse();
		try {
			System.out.println("The application is started..the request hit.."+tableName);
			ckService.updateTableWithEntityID(tableName,current,finalNumber);
				
			return ResponseEntity.ok("Done with update");
		}catch(Exception e) {
			//wResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			//wResponse.setPayload(new ErrorMessageResponse(e.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(e.getMessage());
		}		
	}
	
	private Boolean canProceed(String bearerToken) {
		Integer userId = -1;
		Integer roleId = -1;	
		Calendar currentTime = Calendar.getInstance();
		Calendar dateTime = Calendar.getInstance();
		try {
			System.out.println(bearerToken);
			StringTokenizer bearerTokenizer = new StringTokenizer(bearerToken," ");
			bearerTokenizer.nextToken();
			String token = bearerTokenizer.nextToken();
			System.out.println("The token is "+token);
			String decryptedToken = CryptoHelper.decrypt(token);
			StringTokenizer tokenizer = new StringTokenizer(decryptedToken,":");
			if(tokenizer.hasMoreTokens())  userId = Integer.parseInt(tokenizer.nextToken());
			if(tokenizer.hasMoreTokens())  roleId = Integer.parseInt(tokenizer.nextToken());
			if(tokenizer.hasMoreTokens())  {
				 dateTime.setTimeInMillis(Long.parseLong(tokenizer.nextToken()));
			}
			dateTime.add(Calendar.MINUTE, 120);
			if(dateTime.before(currentTime)) {
				System.out.println(dateTime +" is before "+currentTime);
				return false;
			}else {
				System.out.println(dateTime +" is not before "+currentTime);
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}	
		
	}
/*	@RequestMapping(value="/searchVolunteers",method=RequestMethod.GET, consumes = {JSON_TYPE})
	public ResponseEntity<?> searchVolunteers(@RequestParam(required=false) String term){
//		try {
//			List<Map<String, Object>> response = VolunteerService.findAll(bucket, term);
//			return ResponseEntity.ok(response);
//		}catch(Exception e) {
//			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
//					body(e.getMessage());
//		}
		return null;
	}*/
	
}
