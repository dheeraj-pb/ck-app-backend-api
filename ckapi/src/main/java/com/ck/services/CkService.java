package com.ck.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ck.models.*;

import com.ck.repository.*;
import com.ck.utils.CryptoHelper;

@Service
public class CkService {
	
	@Autowired
	VolunteerRepository volunteerRepository;
	@Autowired
	GroupRepository groupRepository;
	@Autowired
	MessageRepository messageRepository;
	@Autowired
	UserRoleRepository userRoleRepository;
	@Autowired
	UserGroupRepository userGroupRepository;
	@Autowired
	LocationRepository locationRepository;
	@Autowired
	SkillRepository skillRepository;
	@Autowired
	LoginRepository loginRepository;
	
	/**
	 * TODO:Need to implement the password hashing and also try to implement more strong crypto
	 * The logic is that the token will contain the userid, roleid and time. So when validating the
	 * token, we could see if the difference between the current time and the token time has exceeded
	 * the valid time. This is a simple mechanism of login. The token is expected to be passed in the
	 * header of each request.
	 * Role validation also need to be done. it is a TODO
	 * @param request
	 * @return
	 */
	public String login(LoginRequest request) {
		System.out.println("Reached the login in CkService");
		Login result = null;
		try {
			Login login = new Login();
			Calendar dateTime = Calendar.getInstance();
			String tokenString = request.getUserId()+":"+request.getRoleId()+":"+dateTime.getTimeInMillis();
			System.out.println(tokenString);
			String token = CryptoHelper.encrypt(tokenString);
			Volunteer user = volunteerRepository.findVolunteer(request.getUserId());
			System.out.println("Th user in the repo is "+user);
			if((user.getId()==request.getUserId())&&((user.getPassword()==null)||(user.getPassword().equals(request.getPassword())))) {
				login.setUserId(request.getUserId());
				login.setRoleId(request.getRoleId());
				login.setToken(token);
				Login loginResponse = loginRepository.findLogin(request.getUserId(), request.getRoleId());
				if(loginResponse!=null) {
					loginRepository.delete(loginResponse);					
				}
				result = loginRepository.save(login);
			}		
			 
		}catch(Exception e) {
			e.printStackTrace();
		}	
		System.out.println("Got  back the value "+result);
		if(result!=null) return result.getToken();
		
		return new String();
		
	}
	public List<VolunteerBasicModel> getAllVolunteers() {
		System.out.println("Reached the getVolunteers in VolunteerService");
		List<Volunteer> volunteerList = volunteerRepository.findAllVolunteers("%volunteer%");
		List<VolunteerBasicModel> basicList = new ArrayList<VolunteerBasicModel>();
		if(volunteerList != null) {
			for(Volunteer volunteer:volunteerList) {
				VolunteerBasicModel model = new VolunteerBasicModel();
				model.setId(volunteer.getId());
				model.setDistrict(volunteer.getDistrict());
				model.setName(volunteer.getName());
				model.setSkill(volunteer.getSkill());
				basicList.add(model);
				
			}
		}		
		System.out.println("Got  back the list "+volunteerList);
		return basicList;		
		
	}
	
	public List<Group> getAllGroups() {
		System.out.println("Reached the getVolunteers in VolunteerService");
		List<Group> groups = groupRepository.findAllGroups();
		
		System.out.println("Got  back the list "+groups);
		return groups;		
		
	}
	public Volunteer getVolunteerProfile(Integer userId) {
		System.out.println("Reached the getVolunteerProfile in VolunteerService");
		Volunteer volunteer = volunteerRepository.findVolunteer(userId);
		
		System.out.println("Got  back the value "+volunteer);
		return volunteer;		
		
	}
	
	public Group getGroupDetails(Integer groupId) {
		System.out.println("Reached the getVolunteerProfile in VolunteerService");
		Group group = groupRepository.findGroup(groupId);
		
		System.out.println("Got  back the value "+group);
		return group;		
		
	}
	
	public List<Message> getMessagesForGroup(Integer groupId) {
		System.out.println("Reached the getVolunteerProfile in VolunteerService");
		List<Message> messages = messageRepository.findAllMessagesForGroup(groupId);
		
		System.out.println("Got  back the value "+messages);
		return messages;		
		
	}
	
	public List<Message> getMessagesValidForToday(Integer groupId) {
		System.out.println("Reached the getVolunteerProfile in VolunteerService");
		Calendar cal = Calendar.getInstance();
		Date date = new Date(cal.getTimeInMillis());
		List<Message> messages = messageRepository.findAllMessagesForGroupWithDate(groupId,date);
		
		System.out.println("Got  back the value "+messages);
		return messages;		
		
	}
	
	public List<Message> getMessagesValidForDate(Integer groupId,Date date) {
		System.out.println("Reached the getMessagesValidForDate in CkService"+groupId+" and date is "+date);
		
		List<Message> messages = messageRepository.findAllMessagesForGroupWithDate(groupId,date);
		
		System.out.println("Got  back the value "+messages);
		return messages;		
		
	}
	
	
	
	public List<Location> getLocations() {
		System.out.println("Reached the getLocations in CkService");
		List<Location> locations = locationRepository.listLocations();
		
		System.out.println("Got  back the value "+locations);
		return locations;		
		
	}
	
	public List<Skill> getSkills() {
		System.out.println("Reached the getLocations in CkService");
		List<Skill> skills = skillRepository.listSkills();
		
		System.out.println("Got  back the value "+skills);
		return skills;		
		
	}
	public List<Volunteer> listUsersForGroup(Integer groupId) {
		System.out.println("Reached the getLocations in CkService");
		List<UserGroup> userGroups = userGroupRepository.findByGroup(groupId);
		List<Volunteer> users = new ArrayList<Volunteer>();
		if(userGroups !=null) {
			for(UserGroup ug : userGroups) {
				Volunteer user  = volunteerRepository.findVolunteer(ug.getUserId());
				users.add(user);
			}
		}			
		
		System.out.println("Got  back the value "+users);
		return users;		
		
	}
	
	
	
	public List<GroupWithMessageAndParticipants> listGroupsWithMessageForUser(Integer userId) {
		System.out.println("Reached the listGroupsWithMessageForUser in CkService");
		
		List<UserGroup> userGroups = userGroupRepository.findByUser(userId);
		
		List<GroupWithMessageAndParticipants> groupWithMessageAndUsers = new ArrayList<GroupWithMessageAndParticipants>();
		if(userGroups !=null) {
			for(UserGroup userGroup: userGroups) {
				System.out.println("The group id is "+userGroup.getGroupId());
				if(userGroup.getGroupId() !=null) {
					GroupWithMessageAndParticipants groupWithMessageAndUser = new GroupWithMessageAndParticipants();
					Group group = groupRepository.findGroup(userGroup.getGroupId());
					System.out.println("got the group for listGroupsWithMessageForUser in CkService");
					List<Message> messages = messageRepository.findAllMessagesForGroup(userGroup.getGroupId());
					System.out.println("Reached the messages of listGroupsWithMessageForUser in CkService");
					List<UserGroup> usersForGroup = userGroupRepository.findByGroup(userGroup.getGroupId());
					System.out.println("Reached the usersForGroup for listGroupsWithMessageForUser in CkService");
					List<Integer> userIds = new ArrayList<Integer>();
					for(UserGroup ug : usersForGroup) {
						userIds.add(ug.getUserId());
					}
					System.out.println("going to call the findAllbyId listGroupsWithMessageForUser in CkService");
					List<Volunteer> users = volunteerRepository.findVolunteersByIds(userIds);
					System.out.println("Reached findAllbyId the listGroupsWithMessageForUser in CkService");
					groupWithMessageAndUser.setCreatedBy(group.getCreatedBy());
					groupWithMessageAndUser.setCreatedDate(group.getCreatedDate());
					groupWithMessageAndUser.setFromDate(group.getFromDate());
					groupWithMessageAndUser.setGroupDetails(group.getGroupDetails());
					groupWithMessageAndUser.setGroupId(group.getId());
					groupWithMessageAndUser.setGroupName(group.getGroupName());
					groupWithMessageAndUser.setStatus(group.getStatus());
					groupWithMessageAndUser.setToDate(group.getToDate());
					groupWithMessageAndUser.setMessages(messages);
					groupWithMessageAndUser.setUsers(users);
					groupWithMessageAndUsers.add(groupWithMessageAndUser);
				}
								
			}
		}	
			
		
		System.out.println("Got  back the value "+groupWithMessageAndUsers);
		return groupWithMessageAndUsers;		
		
	}
	
	
	
	public Long broadcastMessageToGroup(BroadcastMessageRequest request) {
		System.out.println("Reached the BroadcastMessageToGroup in VolunteerService");
		Message result = null;
		try {
			Message message = new Message();
			message.setMessage(request.getMessage());
			message.setGroupId(request.getGroupId());
			message.setFromDate(request.getFromDate());
			message.setToDate(request.getToDate());
			 result = messageRepository.save(message);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		System.out.println("Got  back the value "+result);
		if(result!=null) return result.getId();
		
		return new Long(-1);		
		
	}
	
	public Long addGroup(AddGroupRequest request) {
		System.out.println("Reached the BroadcastMessageToGroup in VolunteerService");
		Group result = null;
		try {
			Group group = new Group();
			group.setCreatedBy(request.getCreatedBy());
			group.setCreatedDate(request.getCreatedDate());
			group.setFromDate(request.getFromDate());
			group.setGroupDetails(request.getGroupDetails());
			group.setGroupName(request.getGroupName());
			group.setStatus(request.getStatus());
			group.setToDate(request.getToDate());
			 result = groupRepository.save(group);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		System.out.println("Got  back the value "+result);
		if(result!=null) return result.getId();
		
		return new Long(-1);		
		
	}
	
	public Long assignUsersToGroup(AssignUserToGroupRequest request) {
		System.out.println("Reached the AddUserAsVolunteer in VolunteerService");
		UserGroup result=null;
		try {
			int size = request.getUserId().size();			
			for(int i=0;i<size;i++) {
				UserGroup userGroup = new UserGroup();
				userGroup.setGroupId(request.getGroupId());
				userGroup.setUserId(request.getUserId().get(i));
			    result = userGroupRepository.save(userGroup);
			}			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("Got  back the value "+result);
		if(result!=null) return request.getGroupId().longValue();
		
		return new Long(-1);
	}
	
	public Long assignGroupsToUser(AssignGroupsToUserRequest request) {
		System.out.println("Reached the AddUserAsVolunteer in VolunteerService"+request.getGroupIds());
		UserGroup result=null;
		try {
			int size = (request.getGroupIds()).size();			
			for(int i=0;i<size;i++) {
				UserGroup userGroup = new UserGroup();
				userGroup.setUserId(request.getUserId());
				userGroup.setGroupId(request.getGroupIds().get(i));
			    result = userGroupRepository.save(userGroup);
			}			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("Got  back the value "+result);
		if(result!=null) return request.getUserId().longValue();
		
		return new Long(-1);
	}
	
	
	public void removeUserFromGroup(RemoveUserFromGroupRequest request) {
		System.out.println("Reached the AddUserAsVolunteer in VolunteerService");
		UserGroup result=null;
		try {
			UserGroup userGroup = new UserGroup();
			userGroup.setGroupId(request.getGroupId());
			userGroup.setUserId(request.getUserId());
		    userGroupRepository.deleteUserFromGroup(request.getUserId(),request.getGroupId());
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("Got  back the value "+result);		
	}
	
	public Long assignRoleToUser(AssignRoleRequest request) {
		System.out.println("Reached the AddUserAsVolunteer in VolunteerService");
		UserRole result=null;
		try {
			UserRole userRole = new UserRole();
			
			userRole.setRoleId(request.getRoleId());
			userRole.setUserId(request.getUserId());
		    result = userRoleRepository.save(userRole);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("Got  back the value "+result);
		if(result!=null) return result.getId();
		
		return new Long(-1);
	}
	
	public void removeRoleFromUser(RemoveRoleFromUserRequest request) {
		System.out.println("Reached the removeRoleFromUser in CkService");
		
		try {
			UserRole userRole = new UserRole();
			userRole.setRoleId(request.getRoleId());
			userRole.setUserId(request.getUserId());
		    userRoleRepository.delete(userRole);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("Got  back the value ");		
	}
	
	public Long addUser(AddVolunteerRequest request) {
		System.out.println("Reached the AddUserAsVolunteer in VolunteerService");
		Volunteer result=null;
		try {
			Volunteer volunteer = new Volunteer();			
			volunteer.setAddress(request.getAddress());
			volunteer.setAvailableTimings(request.getAddress());
			volunteer.setDistrict(request.getDistrict());
			volunteer.setDob(Date.valueOf(request.getDob()));
			volunteer.setEmail(request.getEmail());
			volunteer.setGender(request.getGender());
			volunteer.setInstitution(request.getInstitution());
			volunteer.setJoiningDate(Date.valueOf(request.getJoiningDate()));
			volunteer.setMobile(request.getMobile());
			volunteer.setName(request.getName());
			volunteer.setPassword(request.getPassword());
			volunteer.setProfession(request.getProfession());
			volunteer.setSkill(request.getSkill());		
			result = volunteerRepository.save(volunteer);			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Got  back the value "+result);
		if(result!=null) return result.getId();
		
		return new Long(-1);		
		
	}
	
	
	
	public void updateTableWithEntityID(String tableName,Integer currentValue,Integer finalValue) {
		System.out.println("Reached the getVolunteerProfile in VolunteerService");
		int loopNumber = finalValue-currentValue;
		Volunteer volunteer = new Volunteer();
		volunteer.setName("test");
		UserRole ur = new UserRole();
		ur.setUserId(1);
		UserGroup ug = new UserGroup();
		ug.setUserId(1);
		Group group = new Group();
		group.setGroupName("x");
		Location location = new Location();
		location.setLocationName("x");
		Login login = new Login();
		login.setUserId(1);
		Message message = new Message();
		message.setGroupId(1);
		Skill skill = new Skill();
		skill.setSkillName("x");
		
		
		
		
		switch(tableName) {
		case "ckuser": for(int i=0;i<loopNumber;i++) {
			              try {
			              volunteerRepository.save(volunteer);
			              }catch(Exception e) {
			            	  System.out.println(e.getMessage());
			              }
	                	}
		                break;
		case "ckuserrole" :  for(int i=0;i<loopNumber;i++) {
			try {
				userRoleRepository.save(ur);
			}catch(Exception e) {
				System.out.println(e.getMessage());
			}            
    	}
        break;      
		case "ckusergroup" :  for(int i=0;i<loopNumber;i++) {
			try {
				userGroupRepository.save(ug);
			}catch(Exception e) {
				System.out.println(e.getMessage());
			}            
    	}
        break;    
		case "ckgroup" :  for(int i=0;i<loopNumber;i++) {
			try {
				groupRepository.save(group);
			}catch(Exception e) {
				System.out.println(e.getMessage());
			}            
    	}
        break;    
		case "cklocation" :  for(int i=0;i<loopNumber;i++) {
			try {
				locationRepository.save(location);
			}catch(Exception e) {
				System.out.println(e.getMessage());
			}            
    	}
        break;    
		case "ckmessage" :  for(int i=0;i<loopNumber;i++) {
			try {
				messageRepository.save(message);
			}catch(Exception e) {
				System.out.println(e.getMessage());
			}            
    	}
        break;    
		case "ckskill" :  for(int i=0;i<loopNumber;i++) {
			try {
				skillRepository.save(skill);
			}catch(Exception e) {
				System.out.println(e.getMessage());
			}            
    	}
        break;    
		   
		 
		}
		
		
		System.out.println("Done with update");				
		
	}
	
	
	
	/*public static List<Map<String, Object>> findAll(final Bucket bucket, String term){
		Statement query;
		String searchField = "type";
		AsPath prefix = select("*").from(i(bucket.name()));
		if (term == null || "".equals(term)) {
			query = select("*").from(i(bucket.name()));
		}else {
			query = prefix.where(i(searchField).like(s(term + "%")));
		}
		
		N1qlQueryResult result = bucket.query(query);
		List<Map<String, Object>> response = extractResultOrThrow(result);
		return response;
	}
	
    private static List<Map<String, Object>> extractResultOrThrow(N1qlQueryResult result) {
        if (!result.finalSuccess()) {
//            LOGGER.warn("Query returned with errors: " + result.errors());
            throw new DataRetrievalFailureException("Query error: " + result.errors());
        }

        List<Map<String, Object>> content = new ArrayList<Map<String, Object>>();
        for (N1qlQueryRow row : result) {
            content.add(row.value().toMap());
        }
        return content;
    }*/
}
