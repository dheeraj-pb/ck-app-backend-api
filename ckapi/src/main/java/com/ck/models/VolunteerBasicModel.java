/**
 * 
 */
package com.ck.models;

import java.io.Serializable;

/**
 * @author Vinod_S08
 *
 */
public class VolunteerBasicModel implements Serializable {
	private long id; 
	private String name;
	private String skill;
	private String district;
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the skill
	 */
	public String getSkill() {
		return skill;
	}
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(String skill) {
		this.skill = skill;
	}
	/**
	 * @return the location
	 */
	public String getDistrict() {
		return district;
	}
	/**
	 * @param location the location to set
	 */
	public void setDistrict(String district) {
		this.district = district;
	}
}
