/**
 * 
 */
package com.ck.models;

import java.util.List;

/**
 * @author Vinod_S08
 *
 */
public class AssignGroupsToUserRequest {

	   Integer userId;
	   List<Integer> groupIds;
	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	 * @return the groupIds
	 */
	public List<Integer> getGroupIds() {
		return groupIds;
	}
	/**
	 * @param groupIds the groupIds to set
	 */
	public void setGroupIds(List<Integer> groupIds) {
		this.groupIds = groupIds;
	}
}
