/**
 * 
 */
package com.ck.models;

import com.ck.interfaces.BaseResponse;

/**
 * @author Vinod_S08
 *
 */
public class ErrorMessageResponse implements BaseResponse {
	
	private String message;
	
	public ErrorMessageResponse() {
		
	}
	
	public ErrorMessageResponse(String message) {
		this.message = message;
	}
	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
