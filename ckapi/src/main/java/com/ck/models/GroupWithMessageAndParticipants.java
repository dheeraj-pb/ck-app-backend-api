/**
 * 
 */
package com.ck.models;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;

/**
 * @author Vinod_S08
 *
 */
public class GroupWithMessageAndParticipants {
	
	    private long groupId;    
		
	    private String groupName;
	  
	    private String createdBy;
	    
	    private Date fromDate;

	    private String status ;
	    
	    
	    private String groupDetails;
	   
	    
	    private Date toDate;
	   
	    private Date createdDate;
	    
	    List<Message> messages;
	    List<Volunteer> users;
		/**
		 * @return the groupId
		 */
		public long getGroupId() {
			return groupId;
		}
		/**
		 * @param groupId the groupId to set
		 */
		public void setGroupId(long groupId) {
			this.groupId = groupId;
		}
		/**
		 * @return the groupName
		 */
		public String getGroupName() {
			return groupName;
		}
		/**
		 * @param groupName the groupName to set
		 */
		public void setGroupName(String groupName) {
			this.groupName = groupName;
		}
		/**
		 * @return the createdBy
		 */
		public String getCreatedBy() {
			return createdBy;
		}
		/**
		 * @param createdBy the createdBy to set
		 */
		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}
		/**
		 * @return the fromDate
		 */
		public Date getFromDate() {
			return fromDate;
		}
		/**
		 * @param fromDate the fromDate to set
		 */
		public void setFromDate(Date fromDate) {
			this.fromDate = fromDate;
		}
		/**
		 * @return the status
		 */
		public String getStatus() {
			return status;
		}
		/**
		 * @param status the status to set
		 */
		public void setStatus(String status) {
			this.status = status;
		}
		/**
		 * @return the groupDetails
		 */
		public String getGroupDetails() {
			return groupDetails;
		}
		/**
		 * @param groupDetails the groupDetails to set
		 */
		public void setGroupDetails(String groupDetails) {
			this.groupDetails = groupDetails;
		}
		/**
		 * @return the toDate
		 */
		public Date getToDate() {
			return toDate;
		}
		/**
		 * @param toDate the toDate to set
		 */
		public void setToDate(Date toDate) {
			this.toDate = toDate;
		}
		/**
		 * @return the createdDate
		 */
		public Date getCreatedDate() {
			return createdDate;
		}
		/**
		 * @param createdDate the createdDate to set
		 */
		public void setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
		}
		/**
		 * @return the messages
		 */
		public List<Message> getMessages() {
			return messages;
		}
		/**
		 * @param messages the messages to set
		 */
		public void setMessages(List<Message> messages) {
			this.messages = messages;
		}
		/**
		 * @return the users
		 */
		public List<Volunteer> getUsers() {
			return users;
		}
		/**
		 * @param users the users to set
		 */
		public void setUsers(List<Volunteer> users) {
			this.users = users;
		}
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "GroupWithMessageAndParticipants [groupId=" + groupId + ", groupName=" + groupName + ", createdBy="
					+ createdBy + ", fromDate=" + fromDate + ", status=" + status + ", groupDetails=" + groupDetails
					+ ", toDate=" + toDate + ", createdDate=" + createdDate + ", messages=" + messages + ", users="
					+ users + "]";
		}
	    

}
