/**
 * 
 */
package com.ck.models;

import java.util.List;

import com.ck.interfaces.BaseResponse;

/**
 * @author Vinod_S08
 *
 */
public class ListGroupsWithMessageResponse implements BaseResponse {
	
	List<GroupWithMessageAndParticipants> groups;

	/**
	 * @return the groups
	 */
	public List<GroupWithMessageAndParticipants> getGroups() {
		return groups;
	}

	/**
	 * @param groups the groups to set
	 */
	public void setGroups(List<GroupWithMessageAndParticipants> groups) {
		this.groups = groups;
	}

}
