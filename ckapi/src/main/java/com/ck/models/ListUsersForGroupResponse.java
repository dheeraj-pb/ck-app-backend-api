/**
 * 
 */
package com.ck.models;

import java.util.List;

import com.ck.interfaces.BaseResponse;

/**
 * @author Vinod_S08
 *
 */
public class ListUsersForGroupResponse implements BaseResponse {
	
	Integer groupId;
	List<Volunteer> users;
	/**
	 * @return the groupId
	 */
	public Integer getGroupId() {
		return groupId;
	}
	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	/**
	 * @return the users
	 */
	public List<Volunteer> getUsers() {
		return users;
	}
	/**
	 * @param users the users to set
	 */
	public void setUsers(List<Volunteer> users) {
		this.users = users;
	}

}
