/**
 * 
 */
package com.ck.models;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;

import com.ck.interfaces.BaseResponse;

/**
 * @author Vinod_S08
 *
 */
@Component
public class VolunteerSearchResponse implements Serializable,BaseResponse{

	private List<VolunteerBasicModel> volunteers;

	/**
	 * @return the volunteers
	 */
	public List<VolunteerBasicModel> getVolunteers() {
		return volunteers;
	}

	/**
	 * @param volunteers the volunteers to set
	 */
	public void setVolunteers(List<VolunteerBasicModel> volunteers) {
		this.volunteers = volunteers;
	}
}
