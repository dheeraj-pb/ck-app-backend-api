/**
 * 
 */
package com.ck.models;

import com.ck.interfaces.BaseResponse;

/**
 * @author Vinod_S08
 *
 */
public class AssignUserToGroupResponse implements BaseResponse {
	
	private Long id;
	private String message;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
