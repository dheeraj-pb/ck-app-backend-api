/**
 * 
 */
package com.ck.models;

import java.util.List;

import com.ck.interfaces.BaseResponse;

/**
 * @author Vinod_S08
 *
 */
public class GetLocationsResponse implements BaseResponse {
	List<Location> locations;

	/**
	 * @return the locations
	 */
	public List<Location> getLocations() {
		return locations;
	}

	/**
	 * @param locations the locations to set
	 */
	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

}
