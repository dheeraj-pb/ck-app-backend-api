/**
 * 
 */
package com.ck.models;

import com.ck.interfaces.BaseResponse;

/**
 * @author Vinod_S08
 *
 */
public class GetGroupDetailsResponse implements BaseResponse {
	private Group group;

	/**
	 * @return the group
	 */
	public Group getGroup() {
		return group;
	}

	/**
	 * @param group the group to set
	 */
	public void setGroup(Group group) {
		this.group = group;
	}

}
