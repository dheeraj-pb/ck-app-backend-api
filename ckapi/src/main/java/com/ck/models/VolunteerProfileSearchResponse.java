package com.ck.models;

import com.ck.interfaces.BaseResponse;

public class VolunteerProfileSearchResponse implements BaseResponse {
	private Volunteer volunteer;

	/**
	 * @return the volunteer
	 */
	public Volunteer getVolunteer() {
		return volunteer;
	}

	/**
	 * @param volunteer the volunteer to set
	 */
	public void setVolunteer(Volunteer volunteer) {
		this.volunteer = volunteer;
	}
	

}
