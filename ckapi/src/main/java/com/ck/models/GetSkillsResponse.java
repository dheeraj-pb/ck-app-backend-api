package com.ck.models;

import java.util.List;

import com.ck.interfaces.BaseResponse;

public class GetSkillsResponse implements BaseResponse {
	
	List<Skill> skills;

	/**
	 * @return the skills
	 */
	public List<Skill> getSkills() {
		return skills;
	}

	/**
	 * @param skills the skills to set
	 */
	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}
	

}
