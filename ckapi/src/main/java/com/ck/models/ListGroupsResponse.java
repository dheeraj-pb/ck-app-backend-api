package com.ck.models;

import java.util.List;

import com.ck.interfaces.BaseResponse;

public class ListGroupsResponse implements BaseResponse {
	List<Group> groups;

	/**
	 * @return the groups
	 */
	public List<Group> getGroups() {
		return groups;
	}

	/**
	 * @param groups the groups to set
	 */
	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

}
