/**
 * 
 */
package com.ck.models;

import java.util.List;

import javax.persistence.Column;

/**
 * @author Vinod_S08
 *
 */
public class AssignUserToGroupRequest {

	private List<Integer> userId;
	
	private Integer groupId;

	/**
	 * @return the userId
	 */
	public List<Integer> getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(List<Integer> userId) {
		this.userId = userId;
	}

	/**
	 * @return the groupId
	 */
	public Integer getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
}
