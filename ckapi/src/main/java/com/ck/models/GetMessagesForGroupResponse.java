/**
 * 
 */
package com.ck.models;

import java.util.List;

import com.ck.interfaces.BaseResponse;

/**
 * @author Vinod_S08
 *
 */
public class GetMessagesForGroupResponse implements BaseResponse{
	
	List<Message> messages;

	/**
	 * @return the messages
	 */
	public List<Message> getMessages() {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

}
