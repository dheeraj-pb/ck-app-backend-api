/**
 * 
 */
package com.ck.models;

import com.ck.interfaces.BaseResponse;

/**
 * @author Vinod_S08
 *
 */
public class WrapperResponse {
	private String status;
	private BaseResponse payload;
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the payload
	 */
	public BaseResponse getPayload() {
		return payload;
	}
	/**
	 * @param payload the payload to set
	 */
	public void setPayload(BaseResponse payload) {
		this.payload = payload;
	}

}
