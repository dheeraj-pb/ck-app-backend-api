package com.ck.repository;

import org.springframework.data.repository.CrudRepository;

import com.ck.models.Volunteer;

public interface IVolunteerRepository extends CrudRepository<Volunteer, String>  {

}
