package com.ck.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ck.models.Group;
import com.ck.models.UserGroup;
import com.ck.models.Volunteer;

public interface UserGroupRepository extends JpaRepository<UserGroup, Long>{
	
	UserGroup save(UserGroup userGroup);
	
	@Query(value="SELECT u.* FROM ckusergroup u WHERE u.userid=?1",
			nativeQuery=true)
	//@Query(value="SELECT * FROM ckuser",nativeQuery=true)
	List<UserGroup> findByUser(Integer userId);
	
	@Query(value="SELECT u.* FROM ckusergroup u WHERE u.groupid=?1",
			nativeQuery=true)
	//@Query(value="SELECT * FROM ckuser",nativeQuery=true)
	List<UserGroup> findByGroup(Integer groupId);
	
	@Query(value="DELETE FROM ckusergroup u WHERE u.userid=?1 AND u.groupid=?2",
			nativeQuery=true)
	void deleteUserFromGroup(Integer userId,Integer groupId);
	
	

}
