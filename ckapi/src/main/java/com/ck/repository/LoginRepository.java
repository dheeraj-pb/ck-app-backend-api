/**
 * 
 */
package com.ck.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ck.models.Login;

/**
 * @author Vinod_S08
 *
 */
public interface LoginRepository extends JpaRepository<Login, Long>{
	
	@Query(value="SELECT l.* FROM cklogin l WHERE l.userid=?1 AND l.roleid=?2",
			nativeQuery=true)
	Login findLogin(Integer userId,Integer roleId);

}
