package com.ck.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ck.interfaces.GlobalConstants;
import com.ck.models.Volunteer;
import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.EntityDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;
import com.couchbase.client.java.query.N1qlQueryRow;
import com.couchbase.client.java.query.Statement;
import com.couchbase.client.java.query.dsl.path.AsPath;
import com.google.gson.Gson;

import static com.couchbase.client.java.query.Select.select;
import static com.couchbase.client.java.query.dsl.functions.Collections.*;
import static com.couchbase.client.java.query.dsl.Expression.*;

@Repository
public interface VolunteerRepository  extends JpaRepository<Volunteer, Long> { //CrudRepository<Volunteer, Long> {

	@Query(value="SELECT u.* FROM ckuser u ,ckuserrole ur, ckrole r WHERE r.id=ur.roleid AND u.id=ur.userid AND r.rolename like ?1 ",
			nativeQuery=true)
	//@Query(value="SELECT * FROM ckuser",nativeQuery=true)
	List<Volunteer> findAllVolunteers(String typeName);


	@Query(value="SELECT u.* FROM ckuser u WHERE u.id=?1",
			nativeQuery=true)
	//@Query(value="SELECT * FROM ckuser",nativeQuery=true)
	Volunteer findVolunteer(Integer userId);
	
	Volunteer save(Volunteer volunteer);
	
	@Query(value="SELECT u.* FROM ckuser u WHERE u.id IN ?1",
			nativeQuery=true)
	List<Volunteer> findVolunteersByIds(List<Integer> userIds);
	
	
	
	
}
