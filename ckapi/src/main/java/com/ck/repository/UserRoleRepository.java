/**
 * 
 */
package com.ck.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ck.models.Message;
import com.ck.models.UserRole;

/**
 * @author Vinod_S08
 *
 */
public interface UserRoleRepository extends JpaRepository<UserRole, Long>{ 

	UserRole save(UserRole userRole);
}
