/**
 * 
 */
package com.ck.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ck.models.Group;

/**
 * @author Vinod_S08
 *
 */
public interface GroupRepository extends JpaRepository<Group, Long>{
	
	@Query(value="SELECT g.* FROM ckgroup g WHERE g.id=?1",
			nativeQuery=true)
	Group findGroup(Integer groupId);
	
	@Query(value="SELECT g.* FROM ckgroup g",
			nativeQuery=true)
	List<Group> findAllGroups();

}
