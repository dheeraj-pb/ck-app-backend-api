/**
 * 
 */
package com.ck.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.ck.models.Skill;
/**
 * @author Vinod_S08
 *
 */
public interface SkillRepository extends JpaRepository<Skill, Long>{
	
	@Query(value="SELECT s.* FROM ckskill s",
			nativeQuery=true)
	List<Skill> listSkills();

}
