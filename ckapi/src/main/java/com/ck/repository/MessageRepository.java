/**
 * 
 */
package com.ck.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ck.models.Message;

/**
 * @author Vinod_S08
 *
 */
public interface MessageRepository extends JpaRepository<Message, Long>{
	
	@Query(value="SELECT m.* FROM ckmessage m WHERE m.groupid=?1",
			nativeQuery=true)
	List<Message> findAllMessagesForGroup(Integer groupId);
	
	@Query(value="SELECT m.* FROM ckmessage m WHERE m.groupid=?1 AND ((?2 BETWEEN m.fromdate AND m.todate) OR (m.fromdate ISNULL AND m.todate ISNULL))",
			nativeQuery=true)
	List<Message> findAllMessagesForGroupWithDate(Integer groupId,Date date);

}
