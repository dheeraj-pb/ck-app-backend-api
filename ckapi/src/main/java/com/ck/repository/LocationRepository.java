/**
 * 
 */
package com.ck.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.ck.models.Location;

/**
 * @author Vinod_S08
 *
 */
public interface LocationRepository extends JpaRepository<Location, Long>{
	
	@Query(value="SELECT l.* FROM cklocation l",
			nativeQuery=true)
	List<Location> listLocations();

}
